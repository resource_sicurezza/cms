export class MenuOption {
  name: string;
  link: string;
  id: number;
  constructor(name: string, link: string, id: number) {
    this.name = name;
    this.link = link;
    this.id = id;
  }
}

export class MenuOptionHead {
  title: string;
  sub_options: MenuOption[];
  constructor(title: string, sub_options: MenuOption[]) {
    this.title = title;
    this.sub_options = sub_options;
  }
}

