import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuOptionHead } from './menu-option.model';
import { element } from 'protractor';

@Component({
  selector: 'app-menu-option',
  templateUrl: './menu-option.component.html',
  styleUrls: ['./menu-option.component.css']
})
export class MenuOptionComponent implements OnInit {
  @Output() notify: EventEmitter<number>;
  @Input() menuoption: MenuOptionHead;
  @Input() selected: number;
  panelOpenState = false;

  constructor() {
    this.notify = new EventEmitter();
  }

  select(id: HTMLInputElement): void {
    this.notify.emit( parseInt(id.value, 10) );
  }

  ngOnInit() {
  }

}
