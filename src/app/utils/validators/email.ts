import { FormControl } from '@angular/forms';

export function emailValidator(control: FormControl): { [s: string]: boolean } {
  const email_regxp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!control.value.match(email_regxp) ) {
   return {invalidEmail: true};
  }
}
