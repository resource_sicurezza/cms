import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  constructor() { }

  login(token): void {
    document.cookie = 'token=' + token;
    this.isLoggedIn = true;
  }
  logout(): void {
    document.cookie = 'token=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    this.isLoggedIn = false;
  }
  getisLoggedIn(): boolean {
    if (this.getToken()) {
      this.isLoggedIn = true;
      return true;
    }
    this.isLoggedIn = false;
    return false;
  }
  getToken() {
    const cooks = {};
    const coockies = document.cookie.replace(' ', '').split(';');
    coockies.forEach((val) => {
      cooks[val.split('=')[0]] = val.split('=')[1];
    });
    return cooks['token'];
  }
  getHeaders() {
    return {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'AUTHORIZATION': this.getToken()
        }
      )
    };
  }

}
