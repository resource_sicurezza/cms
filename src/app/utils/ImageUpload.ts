export function onFileChange(event) {
  const reader = new FileReader();
  if (event.target.files && event.target.files.length > 0) {
    const file = event.target.files[0];
    reader.readAsDataURL(file);
    return new Promise((resolve, reject) => {
      reader.onload = () => {
        resolve( {file, status: 'ok', value: reader.result.split(',')[1]});
      };
      reader.onerror = () => resolve({file, status: 'error'});
    });
  }
}
