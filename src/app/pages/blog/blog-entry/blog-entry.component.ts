import {Component, OnInit, ViewChild, ElementRef, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material';
import { BlogEntryService } from './blog-entry.service';
import * as Quill from 'quill';
import {MessageComponent} from '../../../utils/message/message.component';
import * as ProgressBarActions from '../../../state/progress_bar/actions';
import {AppStore} from '../../../state/app.store';
import {AppState} from '../../../state/app.reducer';
import {Store} from 'redux';
import {BlogEntry} from './blog-entry.model';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {onFileChange} from '../../../utils/ImageUpload';


@Component({
  selector: 'app-blog-entry',
  templateUrl: './blog-entry.component.html',
  styleUrls: ['./blog-entry.component.css']
})
export class BlogEntryComponent implements OnInit {

  editorBlog: any;
  BlogForm: FormGroup;
  blogEntry: BlogEntry;
  noCache = '?nocache=' + Math.floor(Math.random() * 1000);
  dict_image = {value: '', filename: '', filetype: ''};
  @ViewChild('htmlEditor') htmlEditor: ElementRef;
  constructor(private fb: FormBuilder, @Inject(AppStore) private store: Store<AppState>, private blogEntryService: BlogEntryService,
              private dialog: MatDialog, private route: ActivatedRoute) {
    this.createForm();
    this.blogEntry = new BlogEntry(0, '', '', '');
  }

  ngOnInit() {
    const options = {
      modules: {
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold', 'italic', 'underline'],
          ['image', 'code-block'],
          [{ 'align': [] }],
          [{ 'color': [] }],
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        ]
      },
      theme: 'snow'  // or 'bubble'
    };
    this.editorBlog = new Quill(this.htmlEditor.nativeElement, options);
    this.getBlogEntry();
  }
  renderImage() {
    if ( this.blogEntry.image ) {
      if (this.dict_image.value === '') {
        return this.blogEntry.image + this.noCache;
      } else {
        return 'data:image/jpeg;base64,' + this.dict_image.value;
      }
    }
    if (this.dict_image.value !== '') {
      return 'data:image/jpeg;base64,' + this.dict_image.value;
    }
  }
  onFileChange(event) {
    onFileChange(event)
      .then((data) => {
        this.dict_image.value = data['value'];
        this.dict_image.filename = data['file'].name;
        this.dict_image.filetype = data['file'].type;
      });
  }
  createForm() {
    this.BlogForm = this.fb.group({
      content: ['', Validators.required],
      title: ['', Validators.required],
      image: [null, Validators.required],
    });
  }
  getBlogEntry(): void {
    const async_product = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        if (params.get('id')) {
          return this.blogEntryService.getBlogEntry(params.get('id'));
        }
        return [];
      })
    );
    async_product.subscribe((blogEntry: any) => {
      this.blogEntry = blogEntry;
      this.BlogForm.patchValue({
        title: blogEntry.title,
        content: blogEntry.content,
      });
      this.editorBlog.setContents(JSON.parse(blogEntry.content));
    });
  }
  Make() {
    const json_delta = this.editorBlog.getContents();
    const json_text = JSON.stringify(json_delta);
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.BlogForm.patchValue({
      content: json_text,
      image: this.dict_image,
    });
    if ( this.blogEntry.id ) {
      this.blogEntryService.putBlogEntry(this.BlogForm.value, this.blogEntry.id)
        .subscribe(
          (resp) => {
            this.dialog.open(MessageComponent, {
              width: '250px',
              data: { message: 'Blog actualizado' }
            });
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    } else {
      this.blogEntryService.postBlogEntry(this.BlogForm.value)
        .subscribe(
          (resp) => {
            this.dialog.open(MessageComponent, {
              width: '250px',
              data: { message: 'Blog creado' }
            });
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

}
