import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../../../utils/auth-service/auth.service';
import {HttpClient} from '@angular/common/http';
import { BlogEntry } from './blog-entry.model';

@Injectable({
  providedIn: 'root'
})
export class BlogEntryService {

  private blogEntryUrl = 'http://34.204.74.77:8000/ecommerce/site/blog/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  postBlogEntry (blogEntry: BlogEntry): Observable<BlogEntry> {
    return this.http.post<BlogEntry>(this.blogEntryUrl, blogEntry, this.authService.getHeaders());
  }
  putBlogEntry (blogEntry: BlogEntry, id: number): Observable<BlogEntry> {
    return this.http.put<BlogEntry>(this.blogEntryUrl + id.toString() + '/',
      blogEntry, this.authService.getHeaders());
  }

  getBlogEntry (id: string): Observable<BlogEntry> {
    return this.http.get<BlogEntry>(this.blogEntryUrl + id + '/')
      .pipe(
        catchError(this.handleError('getBlogEntryUrl', null))
      );
  }
}
