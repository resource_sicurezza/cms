import { TestBed, inject } from '@angular/core/testing';

import { ListBlogsService } from './list-blogs.service';

describe('ListBlogsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListBlogsService]
    });
  });

  it('should be created', inject([ListBlogsService], (service: ListBlogsService) => {
    expect(service).toBeTruthy();
  }));
});
