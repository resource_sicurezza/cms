import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { AuthService } from '../../utils/auth-service/auth.service';
import { emailValidator } from '../../utils/validators/email';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  invalidCredentials = true;

  constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router,
              private authService: AuthService) {
    console.log(this.authService.isLoggedIn);
    if (this.authService.getisLoggedIn()) {
      this.router.navigate(['admin']);
    }
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      user: ['', Validators.compose([ Validators.required, emailValidator])],
      password: ['',
        Validators.required,
      ],
    });
  }
  Login() {
    this.loginService.login(this.loginForm.value).subscribe(response => {
      this.invalidCredentials = response.login;
      if (response.login) {
        this.authService.login(response.token);
        this.router.navigate(['admin']);
      }
    });
  }

  ngOnInit() {
  }

}
