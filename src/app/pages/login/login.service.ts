import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class LoginService {

  private loginUrl = 'http://34.204.74.77:8000/ecommerce/client/login/';  // URL to web api

  constructor( private http: HttpClient) { }

  /** POST: add a new hero to the server */
  login (login: any): Observable<any> {
    return this.http.post<any>(this.loginUrl, login, httpOptions);
  }

}
