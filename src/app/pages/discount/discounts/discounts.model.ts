export class Discount {
  constructor(
    public date_begin: string,
    public date_end: string,
    public state: boolean,
    public percentage: number,
    public description: string,
    public product: number,
    public group: number,
  ) {
  }
}


