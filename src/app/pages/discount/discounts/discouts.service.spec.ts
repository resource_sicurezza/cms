import { TestBed, inject } from '@angular/core/testing';

import { DiscoutsService } from './discouts.service';

describe('DiscoutsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DiscoutsService]
    });
  });

  it('should be created', inject([DiscoutsService], (service: DiscoutsService) => {
    expect(service).toBeTruthy();
  }));
});
