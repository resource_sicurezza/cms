import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../../utils/auth-service/auth.service';
import {catchError} from 'rxjs/operators';
import { Discount } from './discounts.model';

@Injectable({
  providedIn: 'root'
})
export class DiscoutsService {

  private groupsUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-discount/';
  private deletediscountsUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-discounts/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getDiscounts (): Observable<Discount[]> {
    return this.http.get<Discount[]>(this.groupsUrl)
      .pipe(
        catchError(this.handleError('getDiscounts', []))
      );
  }

  postDiscount (discount: Discount): Observable<Discount> {
    return this.http.post<Discount>(this.groupsUrl, discount, this.authService.getHeaders());
  }

  putDiscount (discount: Discount, id: number): Observable<Discount> {
    return this.http.put<Discount>(this.groupsUrl + id.toString() + '/',
      discount, this.authService.getHeaders());
  }

  deleteDiscounts (discount: any): Observable<Discount[]> {
    return this.http.put<any>(this.deletediscountsUrl, discount, this.authService.getHeaders());
  }
}
