import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { Discount } from './discounts.model';
import { Group } from '../groups/group.model';
import { SmallProduct } from '../../product/values/values.model';
import * as ProgressBarActions from '../../../state/progress_bar/actions';
import {AppState} from '../../../state/app.reducer';
import {AppStore} from '../../../state/app.store';
import {MatTableDataSource, MatSort} from '@angular/material';
import { DiscoutsService } from './discouts.service';
import { GroupService } from '../groups/group.service';
import { ValuesService } from '../../product/values/values.service';
import {SelectionModel} from '@angular/cdk/collections';
import {Store} from 'redux';
import * as moment from 'moment';
import { Moment } from 'moment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'DD/MM/YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'DD/MM/YYYY',
  },
};


@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class DiscountsComponent implements OnInit {
  discountForm: FormGroup;
  send: Boolean;
  discounts: Discount[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.discounts);
  selection = new SelectionModel<Discount>(true, []);
  update = true;
  products: SmallProduct[];
  groups: Group[];
  start_picker_ng: Moment;
  end_picker_ng: Moment;

  @ViewChild(MatSort) sort: MatSort;

  private _mobileQueryListener: () => void;

  constructor(private discoutsService: DiscoutsService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>,
              private valuesService: ValuesService, private groupService: GroupService) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  createForm() {
    this.discountForm = this.fb.group({
      description: ['', Validators.required],
      date_begin: [(new Date()).toISOString(), Validators.required],
      date_end: [(new Date()).toISOString(), Validators.required],
      state: [false],
      percentage: [0, Validators.required],
      product: [null],
      group: [null]
    });
  }
  addCustomDate(type: string, event: MatDatepickerInputEvent<Moment>) {
    if (type === 'date_begin') {
      this.discountForm.patchValue({
        date_begin: event.value.format('DD-MM-YYYY')
      });
    } else {
      this.discountForm.patchValue({
        date_end: event.value.format('DD-MM-YYYY')
      });
    }
  }
  rebuildForm() {
    this.discountForm.reset({
      description: '',
      date_begin: '',
      date_end: '',
      state: false,
      percentage: 0,
      product: null,
      group: null
    });
    this.start_picker_ng = null;
    this.end_picker_ng = null;
  }
  Update() {
    if (this.discountForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.discoutsService.putDiscount(this.discountForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getStores();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            console.log(responseE);
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }
  Make() {
    if (this.discountForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      console.log(this.discountForm.value);
      this.discoutsService.postDiscount(this.discountForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getStores();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            console.log(responseE);
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }
  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.discoutsService.deleteDiscounts({ 'discounts' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getStores();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  setUpdate() {
    if (this.update) {
      this.discountForm.setValue({
        description: this.selection.selected[0]['description'],
        date_begin: this.selection.selected[0]['date_begin'],
        date_end: this.selection.selected[0]['date_end'],
        state: this.selection.selected[0]['state'],
        percentage: this.selection.selected[0]['percentage'],
        product: this.selection.selected[0]['product'],
        group: this.selection.selected[0]['group']
      });
      this.start_picker_ng = moment(this.selection.selected[0]['date_begin'], 'DD-MM-YYYY');
      this.end_picker_ng = moment(this.selection.selected[0]['date_end'], 'DD-MM-YYYY');
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }
  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getStores();
    this.getProducts();
    this.getGroups();
  }
  getStores(): void {
    this.discoutsService.getDiscounts()
      .subscribe( (discounts) => {
        this.discounts = discounts;
        this.dataSource = new MatTableDataSource(this.discounts);
        this.dataSource.sort = this.sort;
      });
  }
  getProducts(): void {
    this.valuesService.getSmalProduct()
      .subscribe( (products) => {
        this.products = products;
      });
  }
  getGroups(): void {
    this.groupService.getGroups()
      .subscribe( (groups) => {
        this.groups = groups;
        console.log(this.groups);
      });
  }
  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
