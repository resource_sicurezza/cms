import { Injectable } from '@angular/core';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../../../utils/auth-service/auth.service';
import {Observable, of} from 'rxjs';
import { Group } from './group.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private groupsUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-group/';  // URL to web api
  private deletegroupsUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-groups/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getGroups (): Observable<Group[]> {
    return this.http.get<Group[]>(this.groupsUrl)
      .pipe(
        catchError(this.handleError('getCategorie', []))
      );
  }

  postGroup (category: Group): Observable<Group> {
    return this.http.post<Group>(this.groupsUrl, category, this.authService.getHeaders());
  }

  putGroup (category: Group, id: number): Observable<Group> {
    return this.http.put<Group>(this.groupsUrl + id.toString() + '/',
      category, this.authService.getHeaders());
  }

  deleteGroups (category: any): Observable<Group[]> {
    return this.http.put<any>(this.deletegroupsUrl, category, this.authService.getHeaders());
  }
}
