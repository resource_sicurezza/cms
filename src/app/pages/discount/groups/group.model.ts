export class Group {
  constructor(
    public id: number,
    public product: number[],
    public name: string,
    public description: string,
  ) {
  }
}
