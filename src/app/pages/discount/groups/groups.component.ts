import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {AppState} from '../../../state/app.reducer';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import {Store} from 'redux';
import * as ProgressBarActions from '../../../state/progress_bar/actions';
import { GroupService } from './group.service';
import { ValuesService } from '../../product/values/values.service';
import {AppStore} from '../../../state/app.store';
import { Group } from './group.model';
import {MatTableDataSource, MatSort, MatSelect} from '@angular/material';
import {SmallProduct} from '../../product/values/values.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  groupForm: FormGroup;
  send: Boolean;
  groups: Group[];
  displayedColumns: String[];
  smallProduct: SmallProduct[];
  dataSource = new MatTableDataSource(this.groups);
  selection = new SelectionModel<Group>(true, []);
  update = true;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('products_select_list') products_select_list: MatSelect;

  private _mobileQueryListener: () => void;

  constructor(private groupService: GroupService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>, private valuesService: ValuesService) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  createForm() {
    this.groupForm = this.fb.group({
      name: ['', Validators.required],
      description_group: [''],
      product: this.fb.array([
        this.fb.group({
        })
      ])
    });
    this.removeProduct(0);
  }

  rebuildForm() {
    this.groupForm.reset({
      name: '',
      description_group: '',
      product: []
    });
    this.products_select_list.value = undefined;
  }

  Update() {
    const selected_products = this.products_select_list.value;
    selected_products.forEach((val) => {
      this.ProductFormArray.push( this.fb.control(val));
    });
    if (this.groupForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.groupService.putGroup(this.groupForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getGroups();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            responseE.error.forEach((err) => {
              /*this.groupForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );*/
            });
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Make() {
    const selected_products = this.products_select_list.value;
    selected_products.forEach((val) => {
      this.ProductFormArray.push( this.fb.control(val));
    });
    console.log(this.groupForm.value);
    if (this.groupForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.groupService.postGroup(this.groupForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getGroups();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            console.log(responseE);
            /*responseE.error.forEach((err) => {
              this.groupForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            });*/
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.groupService.deleteGroups({ 'groups' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getGroups();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setUpdate() {
    if (this.update) {
      this.groupForm.patchValue({
        name: this.selection.selected[0]['name'],
        description_group: this.selection.selected[0]['description_group'],
      });
      this.products_select_list.value = this.selection.selected[0]['product'];
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }

  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getGroups();
    this.getProduct();
  }

  getGroups(): void {
    this.groupService.getGroups()
      .subscribe( (groups) => {
        this.groups = groups;
        this.dataSource = new MatTableDataSource(this.groups);
        this.dataSource.sort = this.sort;
        console.log(this.groups);
      });
  }

  getProduct() {
    this.valuesService.getSmalProduct()
      .subscribe( (products) => {
        console.log(products);
        this.smallProduct = products;
      });
  }

  removeProduct(i: number) {
    this.ProductFormArray.removeAt(i);
  }

  get ProductFormArray(): FormArray {
    return this.groupForm.get('product') as FormArray;
  }

}
