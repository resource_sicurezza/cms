import { Injectable } from '@angular/core';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../../../utils/auth-service/auth.service';
import { Client } from './client.model';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListClientService {

  clientsUrl = 'http://34.204.74.77:8000/ecommerce/client/list-user/';

  constructor(
    private http: HttpClient, private authService: AuthService) {
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getClients (): Observable<Client[]> {
    return this.http.get<Client[]>(this.clientsUrl,  this.authService.getHeaders() )
      .pipe(
        catchError(this.handleError('getProducts', []))
      );
  }
}
