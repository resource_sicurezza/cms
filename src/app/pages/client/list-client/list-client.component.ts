import {ChangeDetectorRef, Component, OnInit, ViewChild, Inject} from '@angular/core';
import {AppState} from '../../../state/app.reducer';
import {Store} from 'redux';
import {AppStore} from '../../../state/app.store';
import {MediaMatcher} from '@angular/cdk/layout';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import {Client} from './client.model';
import {Router} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import { ListClientService } from './list-client.service';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {

  clients: Client[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.clients);
  selection = new SelectionModel<Client>(true, []);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              private listclientsService: ListClientService,
              media: MediaMatcher, private router: Router,
              @Inject(AppStore) private store: Store<AppState>) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      if (this.mobileQuery.matches) {
        this.displayedColumns = ['name', 'email', 'phone'];
      } else {
        this.displayedColumns = ['name', 'last_name', 'email', 'phone'];
      }
    };
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    if (this.mobileQuery.matches) {
      this.displayedColumns = ['name', 'email', 'phone'];
    } else {
      this.displayedColumns = ['name', 'last_name', 'email', 'phone'];
    }
    this.getClients();
  }

  getClients(): void {
    this.listclientsService.getClients()
      .subscribe( (clients) => {
        this.clients = clients;
        this.dataSource = new MatTableDataSource(this.clients);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
