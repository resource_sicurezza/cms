import { TestBed, inject } from '@angular/core/testing';

import { ListClientService } from './list-client.service';

describe('ListClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListClientService]
    });
  });

  it('should be created', inject([ListClientService], (service: ListClientService) => {
    expect(service).toBeTruthy();
  }));
});
