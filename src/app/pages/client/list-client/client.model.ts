export class Client {
  constructor(
    private name: string,
    private last_name: string,
    private email: string,
    private phone: string,
  ) {
  }
}
