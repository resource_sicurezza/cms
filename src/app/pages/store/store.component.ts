import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {AppState} from '../../state/app.reducer';
import * as ProgressBarActions from '../../state/progress_bar/actions';
import {Store} from 'redux';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import {AppStore} from '../../state/app.store';
import { StoreService } from './store.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { StoreLocation } from './store.model';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  storeForm: FormGroup;
  send: Boolean;
  stores: StoreLocation[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.stores);
  selection = new SelectionModel<StoreLocation>(true, []);
  update = true;
  @ViewChild(MatSort) sort: MatSort;

  private _mobileQueryListener: () => void;

  constructor(private storesService: StoreService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  createForm() {
    this.storeForm = this.fb.group({
      name: ['', Validators.required],
      location: this.fb.group({
        lat: [0],
        lng: [0]
      }),
      address: [''],
      reference: [''],
      schedule: ['']
    });
  }
  rebuildForm() {
    this.storeForm.reset({
      name: '',
      location: this.fb.group({
        lat: 0,
        lng: 0
      }),
      address: '',
      reference: '',
      schedule: ''
    });
  }
  Update() {
    console.log(this.selection.selected);
    if (this.storeForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.storesService.putStore(this.storeForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getStores();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.storeForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }
  Make() {
    if (this.storeForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.storesService.postStore(this.storeForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getStores();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.storeForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }
  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.storesService.deleteStore({ 'stores' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getStores();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  setUpdate() {
    console.log(this.selection.selected[0]);
    if (this.update) {
      this.storeForm.setValue({
        name: this.selection.selected[0]['name'],
        location: {
          lat: this.selection.selected[0]['location']['lat'],
          lng: this.selection.selected[0]['location']['lng']
        },
        address: this.selection.selected[0]['address'],
        reference: this.selection.selected[0]['reference'],
        schedule: this.selection.selected[0]['schedule']
      });
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }
  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getStores();
  }
  getStores(): void {
    this.storesService.getStores()
      .subscribe( (stores) => {
        this.stores = stores;
        this.dataSource = new MatTableDataSource(this.stores);
        this.dataSource.sort = this.sort;
      });
  }
  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
