class Location {
  constructor(
    private lat: number,
    private lng: number,
  ) {}
}

export class StoreLocation {
  constructor(
    private name: string,
    private location: Location,
    private address: string,
    private reference: string,
    private schedule: string,
  ) {}
}
