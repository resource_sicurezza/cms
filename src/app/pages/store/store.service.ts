import { Injectable } from '@angular/core';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../../utils/auth-service/auth.service';
import {Observable, of} from 'rxjs';
import { StoreLocation } from './store.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private storeUrl = 'http://34.204.74.77:8000/ecommerce/site/store/';
  private deletestoreUrl = 'http://34.204.74.77:8000/ecommerce/site/set-delete-stores/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
  /** GET heroes from the server */
  getStores (): Observable<StoreLocation[]> {
    return this.http.get<StoreLocation[]>(this.storeUrl)
      .pipe(
        catchError(this.handleError('getCategorie', []))
      );
  }
  postStore (store: StoreLocation): Observable<StoreLocation> {
    return this.http.post<StoreLocation>(this.storeUrl, store, this.authService.getHeaders());
  }
  putStore (store: StoreLocation, id: number): Observable<StoreLocation> {
    return this.http.put<StoreLocation>(this.storeUrl + id.toString() + '/',
      store, this.authService.getHeaders());
  }
  deleteStore (store: any): Observable<StoreLocation[]> {
    return this.http.put<any>(this.deletestoreUrl, store, this.authService.getHeaders());
  }
}
