import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Category } from './category.model';
import { AuthService } from '../../../utils/auth-service/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class CategoriesService {
  private categoriesUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-category/';  // URL to web api
  private deletecategoriesUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-categories/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getCategories (): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoriesUrl)
      .pipe(
        catchError(this.handleError('getCategorie', []))
      );
  }

  postCategories (category: Category): Observable<Category> {
    return this.http.post<Category>(this.categoriesUrl, category, this.authService.getHeaders());
  }

  putCategory (category: Category, id: number): Observable<Category> {
    return this.http.put<Category>(this.categoriesUrl + id.toString() + '/',
      category, this.authService.getHeaders());
  }

  deleteCategory (category: any): Observable<Category[]> {
    return this.http.put<any>(this.deletecategoriesUrl, category, this.authService.getHeaders());
  }
}
