import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy, Inject } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import { Store } from 'redux';
import { CategoriesService } from './categories.service';
import { Category } from './category.model';
import { AppStore } from '../../../state/app.store';
import { AppState } from '../../../state/app.reducer';
import * as ProgressBarActions from '../../../state/progress_bar/actions';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categoryForm: FormGroup;
  send: Boolean;
  categories: Category[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.categories);
  selection = new SelectionModel<Category>(true, []);
  update = true;
  category_error = {};
  @ViewChild(MatSort) sort: MatSort;

  private _mobileQueryListener: () => void;

  constructor(private categoriesService: CategoriesService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  createForm() {
    this.categoryForm = this.fb.group({
      name: ['', Validators.required],
      description: ['']
    });
  }

  rebuildForm() {
    this.categoryForm.reset({
      name: '',
      description: ''
    });
  }

  Update() {
    if (this.categoryForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.categoriesService.putCategory(this.categoryForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getCategories();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.categoryForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Make() {
    if (this.categoryForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.categoriesService.postCategories(this.categoryForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getCategories();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.categoryForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.categoriesService.deleteCategory({ 'categories' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getCategories();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setUpdate() {
    if (this.update) {
      this.categoryForm.setValue({
        name: this.selection.selected[0]['name'],
        description: this.selection.selected[0]['description']
      });
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }

  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getCategories();
  }

  getCategories(): void {
    this.categoriesService.getCategories()
        .subscribe( (categories) => {
          this.categories = categories;
          this.dataSource = new MatTableDataSource(this.categories);
          this.dataSource.sort = this.sort;
          console.log(this.categories);
        });
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
