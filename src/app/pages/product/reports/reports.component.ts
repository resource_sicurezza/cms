import { Component, OnInit, ViewChild, ElementRef,
         HostListener, ChangeDetectorRef, HostBinding } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
  } from '@angular/forms';
import * as _ from 'lodash';
import {MediaMatcher} from '@angular/cdk/layout';


@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
})
export class ReportsComponent implements OnInit {
  @ViewChild('chart') el: ElementRef;
  rangeform: FormGroup;
  mobileQuery: MediaQueryList;
  @HostBinding('class.mobile') matchMedia: boolean;
  categories = ['Categoría 1', 'Categoría 2', 'Categoría 3', 'Categoría 4', 'Categoría 5'];

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, fb: FormBuilder, media: MediaMatcher) {
    this.rangeform = fb.group({
      'date_start': ['01/01/01', Validators.required],
      'date_end': ['', Validators.required]
    });
    this.mobileQuery = media.matchMedia('(max-width: 850px)');
    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      this.matchMedia = this.mobileQuery.matches;
    };
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  onSubmit(value: string): void {
    console.log('you submitted value: ', value);
  }

  ngOnInit() {
    this.basicChart();
    this.matchMedia = this.mobileQuery.matches;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    Plotly.Plots.resize(this.el.nativeElement);
  }

  basicChart() {
    const element = this.el.nativeElement;
    const data = [{
      x: ['Producto 1', 'Producto 2', 'Producto 3', 'Producto 4', 'Producto 5'],
      y: [11, 22, 6, 8, 16],
      type: 'bar'
    }];
    const style = {
      margin: { t: 0, l: 0, r: 0, b: 40 },
    };
    Plotly.newPlot( element, data, style, {displayModeBar: false} );
    Plotly.Plots.resize(this.el.nativeElement);
  }

}
