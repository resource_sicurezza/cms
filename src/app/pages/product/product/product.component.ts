import { switchMap } from 'rxjs/operators';
import {Component, OnInit, ElementRef, ViewChild, Inject} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormArray  } from '@angular/forms';
import { ProductService } from './product.service';
import { CategoriesService } from '../categories/categories.service';
import { TagService } from '../tags/tag.service';
import { Category } from '../categories/category.model';
import { MessageComponent } from '../../../utils/message/message.component';
import { Tag } from '../tags/tag.model';
import { onFileChange } from '../../../utils/ImageUpload';
import {Product} from './product.model';
import { MatSelectionList } from '@angular/material';
import * as ProgressBarActions from '../../../state/progress_bar/actions';
import {Store} from 'redux';
import {AppStore} from '../../../state/app.store';
import {AppState} from '../../../state/app.reducer';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productForm: FormGroup;
  tags: Tag[];
  checked = false;
  disabled = false;
  product = new Product(null, '', '', 0, 0, '', false,
  [], [], []);
  categories: Category[];
  dictImage = {1: {value: '', name: '', type: ''}, 2: {value: '', name: '', type: ''},
    3: {value: '', name: '', type: ''}, 4: {value: '', name: '', type: ''}};
  noChache = '?nocache=' + Math.floor(Math.random() * 1000);

  @ViewChild('tags_select_list') tags_select_list: MatSelectionList;
  @ViewChild('categories_select_list') categories_select_list: MatSelectionList;
  @ViewChild('img_canvas') img_canvas: ElementRef;

  constructor(private fb: FormBuilder, private productService: ProductService, private route: ActivatedRoute,
              private categoriesService: CategoriesService, private tagService: TagService,
              @Inject(AppStore) private store: Store<AppState>, public dialog: MatDialog) {
    this.createForm();
  }

  createForm() {
    this.productForm = this.fb.group({
      sku: ['', Validators.required],
      name: ['', Validators.required],
      price: [0, Validators.required],
      stock: [0, Validators.required],
      in_stock: [false, Validators.required],
      product_details: this.fb.group({
        description: ['', Validators.required],
        rich_snippet_description: ['', Validators.required],
        keywords: ['', Validators.required],
        average_rate: [0, Validators.required]
      }),
      image: this.fb.array([
        this.fb.group({
        })
      ]),
      tag: this.fb.array([
        this.fb.group({
        })
      ]),
      category: this.fb.array([
        this.fb.group({
        })
      ]),
      dolar_price: [0, Validators.required]
    });
    this.removeImage(0);
    this.removeCategory(0);
    this.removeTag(0);
  }

  ngOnInit() {
    this.getProduct();
    this.getCategories();
    this.getTags();
  }

  filterSelectedCategories(id) {
    if (this.product.category.indexOf(id) > -1 ) {
      return true;
    }
    return false;
  }

  filterSelectedTags(id) {
    if (this.product.tag.indexOf(id) > -1 ) {
      return true;
    }
    return false;
  }

  change_in_Stock(event) {
    this.productForm.patchValue({
      in_stock: event.checked
    });
  }

  getProduct(): void {
    const async_product = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        if (params.get('id')) {
          return this.productService.getProduct(params.get('id'));
        }
        return [];
      })
    );
    async_product.subscribe((product: any) => {
      this.product = product;
      this.productForm.patchValue({
        name: product.name,
        product_details: {
            description: product.product_details.description,
            rich_snippet_description:  product.product_details.rich_snippet_description,
            keywords: product.product_details.keywords,
            average_rate: product.product_details.average_rate
        },
        price: product.price,
        dolar_price: product.dolar_price,
        stock: product.stock,
        sku: product.sku,
      });
    });
  }
  renderImage(index) {
    const image_from_array = this.product.image.filter(x => x.position === index);
    if ( image_from_array.length ) {
      if (this.dictImage[index].value === '') {
        return image_from_array[0].path + this.noChache;
      } else {
        return 'data:image/jpeg;base64,' + this.dictImage[index].value;
      }
    }
    if (this.dictImage[index].value !== '') {
      return 'data:image/jpeg;base64,' + this.dictImage[index].value;
    }
  }
  cleanImage() {
    Object.keys(this.dictImage).forEach((id) => {
      if (this.dictImage[id]['value'] !== '') {
       this.removeImage(0);
      }
    });
  }

  getCategories(): void {
    this.categoriesService.getCategories()
      .subscribe( (categories) => {
        this.categories = categories;
        console.log(this.categories);
      });
  }
  getTags(): void {
    this.tagService.getTags()
      .subscribe( (tags) => {
        this.tags = tags;
        console.log(this.categories);
      });
  }

  onFileChange(event, id) {
    onFileChange(event)
      .then((data) => {
        this.dictImage[id].value = data['value'];
        this.dictImage[id].name = data['file'].name;
        this.dictImage[id].type = data['file'].type;
      });
  }

  Make() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    const selected_categories = this.categories_select_list.selectedOptions.selected;
    const selected_tags = this.tags_select_list.selectedOptions.selected;
    selected_categories.forEach((val) => {
      this.CategoryFormArray.push( this.fb.control(val.value));
    });
    selected_tags.forEach((val) => {
      this.TagFormArray.push( this.fb.control(val.value));
    });
    Object.keys(this.dictImage).forEach((id) => {
      if (this.dictImage[id]['value'] !== '') {
        this.ImageFormArray.push(this.fb.group({
          path: {
            filename: this.dictImage[id]['name'],
            filetype: this.dictImage[id]['type'],
            value: this.dictImage[id]['value']
          },
          position: id
        }));
      }
    });
    if ( this.product.id ) {
      this.productService.putProduct(this.productForm.value, this.product.id)
        .subscribe(
          (resp) => {
            console.log(resp);
            this.dialog.open(MessageComponent, {
              width: '250px',
              data: { message: 'Producto actualizado' }
            });
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.cleanImage();
          },
          (responseE) => {
            console.log(responseE);
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    } else {
      this.productService.postProduct(this.productForm.value)
        .subscribe(
          (resp) => {
            console.log(resp);
            this.dialog.open(MessageComponent, {
              width: '250px',
              data: { message: 'Producto creado' }
            });
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.cleanImage();
          },
          (responseE) => {
            console.log(this.productForm.value);
            console.log(responseE);
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  removeImage(i: number) {
    this.ImageFormArray.removeAt(i);
  }

  get ImageFormArray(): FormArray {
    return this.productForm.get('image') as FormArray;
  }

  removeTag(i: number) {
    this.TagFormArray.removeAt(i);
  }

  get TagFormArray(): FormArray {
    return this.productForm.get('tag') as FormArray;
  }

  removeCategory(i: number) {
    this.CategoryFormArray.removeAt(i);
  }

  get CategoryFormArray(): FormArray {
    return this.productForm.get('category') as FormArray;
  }


}
