
class Image {
  constructor(
    public path: string,
    public type: string
  ) {
  }
}


export class Product {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public price: number,
    public dolar_price: number,
    public sku: string,
    public in_stock: boolean,
    public category: number[],
    public tag: number[],
    public image: any[],
  ) {
  }
}
