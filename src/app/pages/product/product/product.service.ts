import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product } from './product.model';
import { AuthService } from '../../../utils/auth-service/auth.service';
import { HttpClient } from '@angular/common/http';
import {Category} from '../categories/category.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-product/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getProduct (id: string): Observable<Product> {
    return this.http.get<Product>(this.productUrl + id + '/')
      .pipe(
        catchError(this.handleError('getProduct', null))
      );
  }

  postProduct (attribute: Product): Observable<Product> {
    return this.http.post<Product>(this.productUrl, attribute, this.authService.getHeaders());
  }

  putProduct (attribute: Product, id: number): Observable<Product> {
    return this.http.put<Product>(this.productUrl + id.toString() + '/',
      attribute, this.authService.getHeaders());
  }
}
