import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Attribute } from './attribute.model';
import { AuthService } from '../../../utils/auth-service/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  private attributesUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-attribute/';  // URL to web api
  private deleteattributesUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-attributes/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getAttributes (): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(this.attributesUrl)
      .pipe(
        catchError(this.handleError('getAttributes', []))
      );
  }

  postAttributes (attribute: Attribute): Observable<Attribute> {
    return this.http.post<Attribute>(this.attributesUrl, attribute, this.authService.getHeaders());
  }

  putAttribute (attribute: Attribute, id: number): Observable<Attribute> {
    return this.http.put<Attribute>(this.attributesUrl + id.toString() + '/',
      attribute, this.authService.getHeaders());
  }

  deleteAttributes (attribute: any): Observable<Attribute[]> {
    return this.http.put<any>(this.deleteattributesUrl, attribute, this.authService.getHeaders());
  }
}
