import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy, Inject } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import { Store } from 'redux';
import { AttributeService } from './attribute.service';
import { Attribute } from './attribute.model';
import { AppStore } from '../../../state/app.store';
import { AppState } from '../../../state/app.reducer';
import * as ProgressBarActions from '../../../state/progress_bar/actions';


@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesComponent implements OnInit {
  attributeForm: FormGroup;
  send: Boolean;
  attributes: Attribute[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.attributes);
  selection = new SelectionModel<Attribute>(true, []);
  update = true;
  category_error = {};
  @ViewChild(MatSort) sort: MatSort;

  types = [
    {value: 'CL', viewValue: 'COLOR'},
    {value: 'OT', viewValue: 'OTRO'},
  ];

  private _mobileQueryListener: () => void;

  constructor(private attributeService: AttributeService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  createForm() {
    this.attributeForm = this.fb.group({
      name: ['', Validators.required],
      type: ['', Validators.required]
    });
  }

  rebuildForm() {
    this.attributeForm.reset({
      name: '',
      type: ''
    });
  }

  Update() {
    if (this.attributeForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.attributeService.putAttribute(this.attributeForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getAttributes();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.attributeForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Make() {
    if (this.attributeForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.attributeService.postAttributes(this.attributeForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getAttributes();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.attributeForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.attributeService.deleteAttributes({ 'attributes' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getAttributes();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setUpdate() {
    console.log(this.selection.selected);
    if (this.update) {
      this.attributeForm.setValue({
        name: this.selection.selected[0]['name'],
        type: this.selection.selected[0]['type']
      });
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }

  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getAttributes();
  }

  getAttributes(): void {
    this.attributeService.getAttributes()
      .subscribe( (attributes) => {
        this.attributes = attributes;
        this.dataSource = new MatTableDataSource(this.attributes);
        this.dataSource.sort = this.sort;
        console.log(this.attributes);
      });
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
