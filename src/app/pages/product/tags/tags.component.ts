import { Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy, Inject } from '@angular/core';
import { Tag } from './tag.model';
import {SelectionModel} from '@angular/cdk/collections';
import {Store} from 'redux';
import * as ProgressBarActions from '../../../state/progress_bar/actions';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { TagService } from './tag.service';
import {AppStore} from '../../../state/app.store';
import {AppState} from '../../../state/app.reducer';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  tagForm: FormGroup;
  send: Boolean;
  tags: Tag[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.tags);
  selection = new SelectionModel<Tag>(true, []);
  update = true;
  category_error = {};
  @ViewChild(MatSort) sort: MatSort;

  private _mobileQueryListener: () => void;

  constructor(private tagsService: TagService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>) {
    this.displayedColumns = ['select', 'name'];
    this.createForm();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  createForm() {
    this.tagForm = this.fb.group({
      name: ['', Validators.required]
    });
  }

  rebuildForm() {
    this.tagForm.reset({
      name: ''
    });
  }

  Update() {
    if (this.tagForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.tagsService.putTag(this.tagForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getTags();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.tagForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Make() {
    if (this.tagForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.tagsService.postTags(this.tagForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getTags();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.tagForm.controls['name'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.tagsService.deleteTag({ 'tags' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getTags();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setUpdate() {
    if (this.update) {
      this.tagForm.setValue({
        name: this.selection.selected[0]['name']
      });
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }

  ngOnInit() {
    this.displayedColumns = ['select', 'name'];
    this.getTags();
  }

  getTags(): void {
    this.tagsService.getTags()
      .subscribe( (tags) => {
        this.tags = tags;
        this.dataSource = new MatTableDataSource(this.tags);
        this.dataSource.sort = this.sort;
        console.log(this.tags);
      });
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
