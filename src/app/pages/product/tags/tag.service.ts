import { Injectable } from '@angular/core';
import { Tag } from './tag.model';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../../utils/auth-service/auth.service';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  private tagsUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-tags/';  // URL to web api
  private deletetagsUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-tags/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getTags (): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.tagsUrl)
      .pipe(
        catchError(this.handleError('getCategorie', []))
      );
  }

  postTags (category: Tag): Observable<Tag> {
    return this.http.post<Tag>(this.tagsUrl, category, this.authService.getHeaders());
  }

  putTag (category: Tag, id: number): Observable<Tag> {
    return this.http.put<Tag>(this.tagsUrl + id.toString() + '/',
      category, this.authService.getHeaders());
  }

  deleteTag (category: any): Observable<Tag[]> {
    return this.http.put<any>(this.deletetagsUrl, category, this.authService.getHeaders());
  }
}
