import {Component, OnInit, ViewChild, ChangeDetectorRef, OnDestroy, Inject, ElementRef} from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MediaMatcher } from '@angular/cdk/layout';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectionModel} from '@angular/cdk/collections';
import { Store } from 'redux';
import { ValuesService } from './values.service';
import { AttributeService } from '../attributes/attribute.service';
import { AttributeValues, SmallProduct, AttributeProduct } from './values.model';
import { Attribute } from '../attributes/attribute.model';
import { AppStore } from '../../../state/app.store';
import { AppState } from '../../../state/app.reducer';
import * as ProgressBarActions from '../../../state/progress_bar/actions';


@Component({
  selector: 'app-values',
  templateUrl: './values.component.html',
  styleUrls: ['./values.component.css']
})
export class ValuesComponent implements OnInit {
  attributeProductForm: FormGroup;
  attributeValueForm: FormGroup;
  attribute_product_selected: AttributeProduct;
  send: Boolean;
  attributesvalue: AttributeValues[];
  smallProduct: SmallProduct[];
  attribute: Attribute[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.attributesvalue);
  selection = new SelectionModel<AttributeValues>(true, []);
  update = true;
  category_error = {};
  attributesvaluePanel = [1];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('values') values: ElementRef;

  private _mobileQueryListener: () => void;

  constructor(private valuesService: ValuesService, private fb: FormBuilder,
              @Inject(AppStore) private store: Store<AppState>,
              private attributeService: AttributeService) {
    this.displayedColumns = ['select', 'product_name', 'attribute_name'];
    this.createForm();
  }

  addAttributeValue() {
    this.AttributeValuesFormArray.push(this.fb.group({
      value: '',
      product_attribute: this.selection.selected[0]['id'],
      delete: false
    }));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  createForm() {
    this.attributeProductForm = this.fb.group({
      attribute: ['', Validators.required],
      product: ['', Validators.required],
    });

    this.attributeValueForm = this.fb.group({
      values: this.fb.array([
        this.fb.group({
          value: ['', Validators.required],
          product_attribute: ['', Validators.required],
          id: [''],
          delete: false,
        })
      ])
    });
    this.removeAttributeValues(0);
  }

  rebuildForm() {
    this.attributeProductForm.reset({
      attribute: '',
      product: ''
    });
    this.attributeValueForm.setControl('values',
      this.fb.array([
        this.fb.group({
          value: '',
          id: '',
          product_attribute: '',
          delete: false,
        })
      ])
      );
    this.removeAttributeValues(0);
  }


  Update() {
    if (this.attributeValueForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.valuesService.putAttributeProduct(this.attributeProductForm.value, this.selection.selected[0]['id'])
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getAttributeValues();
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
            this.selection.clear();
            this.update = true;
          },
          (responseE) => {
            console.log(responseE);
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Make() {
    if (this.attributeProductForm.status === 'VALID' ) {
      this.store.dispatch(ProgressBarActions.show_progres_bar());
      this.valuesService.postAttributeProduct(this.attributeProductForm.value)
        .subscribe(
          (resp) => {
            this.rebuildForm();
            this.getAttributeValues();
            this.selection.clear();
            this.update = true;
            this.attribute_product_selected = resp;
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          },
          (responseE) => {
            for (const err in responseE.error) {
              this.attributeProductForm.controls['attribute'].setErrors(
                {unique: {text: responseE.error[err][0]}}
              );
            }
            console.log(this.attributeProductForm.get('attribute').getError('unique'));
            this.store.dispatch(ProgressBarActions.hide_progress_bar());
          }
        );
    }
  }

  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.valuesService.deleteAttributesValue({ 'product_attributes' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getAttributeValues();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setUpdate() {
    if (this.update) {
      this.attributeProductForm.setValue({
        attribute: this.selection.selected[0]['attribute'],
        product: this.selection.selected[0]['product']
      });
      this.attribute_product_selected = this.attributeProductForm.value;
      this.selection.selected[0]['attribute_values'].forEach((val) => {
        this.AttributeValuesFormArray.push(this.fb.group({
          value: val['value'],
          id: val['id'],
          product_attribute: this.selection.selected[0]['id'],
          delete: false
        }));
        this.attributesvaluePanel.push(1);
      });
    } else {
      this.rebuildForm();
    }
    this.update = !this.update;
  }

  assignValues() {
    this.valuesService.postAttributeValue(this.attributeValueForm.value)
      .subscribe(
        (resp) => {
          this.getAttributeValues();
          this.rebuildForm();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
          this.update = true;
          this.selection.clear();
        },
        (responseE) => {
          console.log(responseE);
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

  ngOnInit() {
    this.displayedColumns = ['select', 'product_name', 'attribute_name'];
    this.getAttributeValues();
    this.getProduct();
    this.getAttributes();
  }

  getAttributeValues(): void {
    this.valuesService.getAttributesValue()
      .subscribe( (attributesV) => {
        this.attributesvalue = attributesV;
        this.dataSource = new MatTableDataSource(this.attributesvalue);
        this.dataSource.sort = this.sort;
        console.log(this.attributesvalue);
      });
  }

  getProduct() {
    this.valuesService.getSmalProduct()
      .subscribe( (products) => {
        this.smallProduct = products;
      });
  }

  getAttributes() {
    this.attributeService.getAttributes()
      .subscribe( (attrs) => {
        this.attribute = attrs;
      });
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  removeAttributeValues(i: number) {
    this.AttributeValuesFormArray.removeAt(i);
  }
  removeAttributeValueForm(i: number) {
    this.AttributeValuesFormArray.controls[i].patchValue({
      'delete': true
    });
  }

  get AttributeValuesFormArray(): FormArray {
    return this.attributeValueForm.get('values') as FormArray;
  }

  filterAttributeValue() {
    return this.AttributeValuesFormArray.controls.filter(
      at_v => at_v.get('delete').value === false
    );
  }

}
