export class Value {
  constructor(
    public value: string,
  ) {
  }
}


export class AttributeValues {
  constructor(
    public id: number,
    public attribute_id: string,
    public attribute: Value[],
    public attribute_name: string,
    public product: number,
  ) {
  }
}

export class AttributeProduct {
  constructor(
    public attribute_id: number,
    public product_id: number,
  ) {
  }
}

export class SmallProduct {
  constructor(
    public id: number,
    public name: string,
  ) {
  }
}
