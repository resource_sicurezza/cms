import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AttributeValues, SmallProduct, AttributeProduct, Value } from './values.model';
import { AuthService } from '../../../utils/auth-service/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ValuesService {

  private attributeProductUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-product-attribute/';
  private attributeValueUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-attribute-value/';
  private smallproductUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-list-small-products/';
  private deleteproductAttributeUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-product-attributes/';

  constructor(
    private http: HttpClient,
    private authService: AuthService) { }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getAttributesValue (): Observable<AttributeValues[]> {
    return this.http.get<AttributeValues[]>(this.attributeProductUrl)
      .pipe(
        catchError(this.handleError('getCategorie', []))
      );
  }
  /** GET heroes from the server */
  getSmalProduct (): Observable<SmallProduct[]> {
    return this.http.get<SmallProduct[]>(this.smallproductUrl)
      .pipe(
        catchError(this.handleError('getSmallProduct', []))
      );
  }
  postAttributeProduct (attributeproduct: AttributeProduct): Observable<AttributeProduct> {
    return this.http.post<AttributeProduct>(this.attributeProductUrl, attributeproduct, this.authService.getHeaders());
  }

  putAttributeProduct (category: AttributeValues, id: number): Observable<AttributeValues> {
    return this.http.put<AttributeValues>(this.attributeProductUrl + id.toString() + '/',
      category, this.authService.getHeaders());
  }

  deleteAttributesValue (attributeValues: any): Observable<AttributeValues[]> {
    return this.http.put<any>(this.deleteproductAttributeUrl, attributeValues, this.authService.getHeaders());
  }
  postAttributeValue (values: any): Observable<Value[]> {
    return this.http.post<any>(this.attributeValueUrl, values, this.authService.getHeaders());
  }
}
