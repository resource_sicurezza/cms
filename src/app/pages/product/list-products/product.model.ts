export class Product {
  id: number;
  name: string;
  category: string;
  inventory: string;

  constructor(id: number, name: string, category: string, inventory: string){
    this.id = id;
    this.name = name;
    this.category = category;
    this.inventory = inventory;
  }
}
