import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '../../../utils/auth-service/auth.service'
import { Product } from './product.model';


@Injectable()
export class ListProductsService {
  private productsUrl = 'http://34.204.74.77:8000/ecommerce/product/admin-list-products/';
  private deleteproductsUrl = 'http://34.204.74.77:8000/ecommerce/product/set-delete-products/';

  constructor(
    private http: HttpClient, private authService: AuthService) {
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** GET heroes from the server */
  getProducts (): Observable<Product[]> {
    return this.http.get<Product[]>(this.productsUrl,  this.authService.getHeaders() )
      .pipe(
        catchError(this.handleError('getProducts', []))
      );
  }
  deleteProduct (product: any): Observable<Product[]> {
    return this.http.put<any>(this.deleteproductsUrl, product, this.authService.getHeaders());
  }
}
