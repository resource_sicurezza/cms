import { Component, OnInit, ViewChild, ChangeDetectorRef, Inject } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { Store } from 'redux';
import { AppStore } from '../../../state/app.store';
import { AppState } from '../../../state/app.reducer';
import { Router } from '@angular/router';
import { ListProductsService } from './list-products.service';
import { Product } from './product.model';
import * as ProgressBarActions from '../../../state/progress_bar/actions';


@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  products: Product[];
  mobileQuery: MediaQueryList;
  displayedColumns: String[];
  dataSource = new MatTableDataSource(this.products);
  selection = new SelectionModel<Product>(true, []);
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              private listProductsService: ListProductsService,
              media: MediaMatcher, private router: Router,
              @Inject(AppStore) private store: Store<AppState>) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      if (this.mobileQuery.matches) {
        this.displayedColumns = ['select', 'name', 'tag'];
      } else {
        this.displayedColumns = ['select', 'name', 'category', 'tag'];
      }
    };
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    if (this.mobileQuery.matches) {
      this.displayedColumns = ['select', 'name', 'tag'];
    } else {
      this.displayedColumns = ['select', 'name', 'category', 'tag'];
    }
    this.getHeroes();
  }

  getHeroes(): void {
    this.listProductsService.getProducts()
        .subscribe( (products) => {
          this.products = products;
          this.dataSource = new MatTableDataSource(this.products);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(this.products);
        });
  }

  JoinMaps(categories) {
    const categoriesJoin = categories.map(ct => ct.name).join(', ');
    console.log(categoriesJoin);
    return categoriesJoin;
  }

  Make() {
    this.router.navigate(['admin/product']);
  }

  OnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  Delete() {
    this.store.dispatch(ProgressBarActions.show_progres_bar());
    this.listProductsService.deleteProduct({ 'products' : this.selection.selected})
      .subscribe(
        (resp) => {
          this.selection.clear();
          this.getHeroes();
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        },
        (responseE) => {
          this.store.dispatch(ProgressBarActions.hide_progress_bar());
        }
      );
  }

}
