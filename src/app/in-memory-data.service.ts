import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const products_list = [
      { id: 11, name: 'Mr. Nice', category: 'Category', inventory: 12 },
      { id: 12, name: 'Narco', category: 'Category', inventory: 12 },
      { id: 13, name: 'Bombasto', category: 'Category', inventory: 12 },
      { id: 14, name: 'Celeritas', category: 'Category', inventory: 12 },
      { id: 15, name: 'Magneta', category: 'Category', inventory: 12 },
      { id: 16, name: 'RubberMan', category: 'Category', inventory: 12 },
      { id: 17, name: 'Dynama', category: 'Category', inventory: 12 },
      { id: 18, name: 'Dr IQ', category: 'Category', inventory: 12 },
      { id: 19, name: 'Magma', category: 'Category', inventory: 12 },
      { id: 20, name: 'Tornado', category: 'Category', inventory: 12 }
    ];
    const categories_list = [
      { id: 11, name: 'Mr. Nice' },
      { id: 12, name: 'Narco' },
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' },
      { id: 16, name: 'RubberMan' },
    ];
    return {products_list, categories_list};
  }
}
