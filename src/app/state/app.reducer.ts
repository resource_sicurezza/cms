/* tslint:disable:typedef */

import {
  Reducer,
  combineReducers
} from 'redux';
import {
  ProgressBarState,
  ProgressBarReducer
} from './progress_bar/reducer';

export interface AppState {
  progress_bar_state: ProgressBarState;
}

const rootReducer: Reducer<AppState> = combineReducers<AppState>({
  progress_bar_state: ProgressBarReducer,
});

export default rootReducer;
