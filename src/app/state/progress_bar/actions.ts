import {
  Action,
  ActionCreator
} from 'redux';


export const SHOW_PROGRESS_BAR = 'SHOW_PROGRESS_BAR';
export const show_progres_bar: ActionCreator<Action> = () => ({
  type: SHOW_PROGRESS_BAR
});

export const HIDE_PROGRESS_BAR = 'HIDE_PROGRESS_BAR';
export const hide_progress_bar: ActionCreator<Action> = () => ({
  type: HIDE_PROGRESS_BAR
});
