import { Action } from 'redux';
import * as ProgressBarActions from './actions';
import { createSelector } from 'reselect';


export interface ProgressBarState {
  progress_bar_state: boolean;
}

const initialState: ProgressBarState = {
  progress_bar_state: false
};

export const ProgressBarReducer =
  function(state: ProgressBarState = initialState, action: Action): ProgressBarState {
    switch (action.type) {
      case ProgressBarActions.SHOW_PROGRESS_BAR:
        return {
          progress_bar_state: true
        };
      case ProgressBarActions.HIDE_PROGRESS_BAR:
        return {
          progress_bar_state: false
        };
      default:
        return state;
    }
  };

export const getProgressBarState = (state): ProgressBarState => state.progress_bar_state;

export const getCurrentProgressBarState = createSelector(
  getProgressBarState,
  ( state: ProgressBarState ) => state.progress_bar_state );
