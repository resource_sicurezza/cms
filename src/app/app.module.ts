import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {
  RouterModule,
  Routes
  } from '@angular/router';

import {
  MatNativeDateModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatSortModule,
  MatButtonModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatTableModule,
  MatCardModule,
  MatIconModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatExpansionModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSnackBarModule
} from '@angular/material';

import {LayoutModule} from '@angular/cdk/layout';
import { MenuOptionComponent } from './menu-option/menu-option.component';
import { ListProductsComponent } from './pages/product/list-products/list-products.component';
import { ReportsComponent } from './pages/product/reports/reports.component';
import { ListProductsService } from './pages/product/list-products/list-products.service';
import { CategoriesService } from './pages/product/categories/categories.service';
import { ProductComponent } from './pages/product/product/product.component';
import { CategoriesComponent } from './pages/product/categories/categories.component';
import { AttributesComponent } from './pages/product/attributes/attributes.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './utils/AuthGuard';
import { AppshellComponent } from './appshell/appshell.component';
import { LoginLayoutComponent } from './pages/login-layout/login-layout.component';
import { ValuesComponent } from './pages/product/values/values.component';
import { TagsComponent } from './pages/product/tags/tags.component';
import { MessageComponent } from './utils/message/message.component';
import { ListClientComponent } from './pages/client/list-client/list-client.component';
import { StoreComponent } from './pages/store/store.component';
import { GroupsComponent } from './pages/discount/groups/groups.component';
import { DiscountsComponent } from './pages/discount/discounts/discounts.component';
import { BlogEntryComponent } from './pages/blog/blog-entry/blog-entry.component';
import { ListBlogsComponent } from './pages/blog/list-blogs/list-blogs.component';


import {
  AppStore,
  appStoreProviders
} from './state/app.store';


const routes: Routes = [
  {
    path: 'admin',
    component: AppshellComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: ListProductsComponent },
      { path: 'blog-entry', component: BlogEntryComponent },
      { path: 'blog-entry/:id', component: BlogEntryComponent },
      { path: 'blog-list', component: ListBlogsComponent },
      { path: 'group', component: GroupsComponent },
      { path: 'discounts', component: DiscountsComponent },
      { path: 'list_products', component: ListProductsComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'stores', component: StoreComponent },
      { path: 'tags', component: TagsComponent },
      { path: 'attributes', component: AttributesComponent },
      { path: 'attribute-values', component: ValuesComponent },
      { path: 'list-client', component: ListClientComponent },
      { path: 'product/:id', component: ProductComponent },
      { path: 'product', component: ProductComponent },
      { path: 'report_products', component: ReportsComponent },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        component: LoginComponent
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuOptionComponent,
    ListProductsComponent,
    ReportsComponent,
    ProductComponent,
    CategoriesComponent,
    AttributesComponent,
    LoginComponent,
    AppshellComponent,
    LoginLayoutComponent,
    ValuesComponent,
    TagsComponent,
    MessageComponent,
    ListClientComponent,
    StoreComponent,
    GroupsComponent,
    DiscountsComponent,
    BlogEntryComponent,
    ListBlogsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    [BrowserAnimationsModule],
    FormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    LayoutModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatExpansionModule,
    MatCardModule,
    MatTableModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatSnackBarModule,
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    /*HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )*/
  ],
  entryComponents: [MessageComponent],
  providers: [AuthGuard, ListProductsService, CategoriesService, appStoreProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
