import { Inject } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {
  Router
} from '@angular/router';
import { Store } from 'redux';
import {ChangeDetectorRef, Component} from '@angular/core';
import { MenuOptionHead, MenuOption } from '../menu-option/menu-option.model';
import { AppStore } from '../state/app.store';
import { AppState } from '../state/app.reducer';

@Component({
  selector: 'app-appshell',
  templateUrl: './appshell.component.html',
  styleUrls: ['./appshell.component.css']
})
export class AppshellComponent {
  mobileQuery: MediaQueryList;
  menuoption: MenuOptionHead[];
  selected = 0;
  progress_bar_state: boolean;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
              private router: Router, @Inject(AppStore) private store: Store<AppState>) {
    store.subscribe(() => this.readState());
    this.readState();
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.menuoption = [
      new MenuOptionHead('Producto', [
        new MenuOption('Lista de productos', '/admin/list_products', 1),
        new MenuOption('Categorías', '/admin/categories', 2),
        new MenuOption('Atributos', '/admin/attributes', 3),
        new MenuOption('Valor de los atributos', '/admin/attribute-values', 4),
        new MenuOption('Tags', '/admin/tags', 5),
        new MenuOption('Reporte de productos', '/admin/report_products', 6),
      ]),
      new MenuOptionHead('Clientes', [
        new MenuOption('Lista de clientes', '/admin/list-client', 7),
      ]),
      new MenuOptionHead('Tiendas', [
        new MenuOption('Lista de Tiendas', '/admin/stores', 8),
      ]),
      new MenuOptionHead('Descuento', [
        new MenuOption('Grupos', '/admin/group', 9),
        new MenuOption('Descuentos', '/admin/discounts', 10),
      ]),
      new MenuOptionHead('Blog', [
        new MenuOption('Blog', '/admin/blog-entry', 11),
      ]),
    ];
  }

  readState() {
    const state: AppState = this.store.getState() as AppState;
    this.progress_bar_state = state.progress_bar_state.progress_bar_state;
  }

  changeSelected(id: number): void {
    this.selected = id;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
